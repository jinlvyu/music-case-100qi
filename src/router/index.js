import Vue from 'vue'
import VueRouter from 'vue-router'
import layout from '@/views/layout'
import play from '@/views/play'
import home from '@/views/home'
import search from '@/views/search'
Vue.use(VueRouter)

const routes = [
  {
    name: 'layout', path: '/', component: layout, redirect: '/home', children: [
      {
        name: 'home', path: 'home', component: home, meta: {
          title: '首页'
        }
      },
      {
        name: 'search', path: 'search', component: search, meta: {
          title: '搜索'
        }
      },
    ]
  },
  { name: 'play', path: '/play', component: play },
]

const router = new VueRouter({
  routes
})

export default router

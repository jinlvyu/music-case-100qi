import request from '@/utils/request'

export const recommendListApi = () => {
  return request.get(`/personalized?limit=6`)
}


export const personalListApi = () => {
  return request.get('/personalized/newsong')
}
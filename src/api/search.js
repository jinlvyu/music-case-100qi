import request from '@/utils/request'
export const searchHotApi = () => request.get('/search/hot')

export const searchMultimatchApi = (params) => {
  return request({
    url: `/search?keywords=${params.keywords}`
  }
  )
}
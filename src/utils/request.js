import axios from 'axios'

// 1.创建副本
const request = axios.create({
  baseURL: 'http://music.zll.cool/'
})
// 2.统一设置拦截器
request.interceptors.request.use((config) => {
  return config
}, (err) => {
  return Promise.reject(err)
})
request.interceptors.response.use((response) => {
  return response
}, (err) => {
  return Promise.reject(err)
})

export default request
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vant from 'vant';
import 'vant/lib/index.css';
import MusicCell from '@/components/MusicCell.vue'
Vue.use(Vant);
Vue.config.productionTip = false
Vue.component('MusicCell', MusicCell)
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
